package com.applicationservice.service.exception;

public class RabbitMQSerderException extends RuntimeException {

    private static final long serialVersionUID = 6825537414445573470L;

    public RabbitMQSerderException(String message) {
        super(message);
    }
}