package com.applicationservice.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.applicationservice.config.ApplicationConstants;
import com.applicationservice.json.Employee;
import com.applicationservice.service.exception.FileUploadFailedException;
import com.applicationservice.service.exception.RabbitMQSerderException;



@Service
public class RabbitMQSender {
	@Autowired
	public AmqpTemplate amqpTemplate;
	
	@Value("${RabbitMq.exchange}")
	private String exchange;
	
	@Value("${RabbitMq.routingkey}")
	private String routingkey;	
	
	
	public boolean send(Employee emp) {
		try {
			amqpTemplate.convertAndSend(exchange, routingkey, emp);
			System.out.println("Send msg = " + emp);
			return true;
		}
		catch (Exception e) {
			throw new RabbitMQSerderException(ApplicationConstants.Sending_RabbitMQ_Failed);
			
		}
		
	    
	}
	
	@RabbitListener(queues = "${RabbitMq.queue}")
	public void recievedMessage(Employee employee) {
		//Object t1=amqpTemplate.receiveAndConvert(queueName, 10000);
		System.out.println("Recieved Message From RabbitMQ: " + employee);
	}

}
