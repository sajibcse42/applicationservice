package com.applicationservice.entity;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ErrorResponse {
	private String title;
    private HttpStatus status;
    private Long timestamp;
}