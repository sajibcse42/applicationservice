package com.applicationservice.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.applicationservice.config.ApplicationConstants;
import com.applicationservice.entity.Medicine;
import com.applicationservice.repository.IMedicineRepository;
import com.applicationservice.service.exception.FileUploadFailedException;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;


@Service
public class UploadService {
	
	@Autowired
	public IMedicineRepository medicineRepository;

	
	public List<Medicine> uploadFile(MultipartFile multipartFile) throws IOException {

		File file = convertMultiPartToFile(multipartFile);

		List<Medicine> mandatoryMissedList = new ArrayList<Medicine>();

		try (Reader reader = new FileReader(file);)
		{
			CsvToBean<Medicine> csvToBean = new CsvToBeanBuilder<Medicine>(reader).withType(Medicine.class)
					.withIgnoreLeadingWhiteSpace(true).build();
			List<Medicine> medicineList = csvToBean.parse();

			Iterator<Medicine> medicineListListClone = medicineList.iterator();

			while (medicineListListClone.hasNext())
			{

				Medicine medicine = medicineListListClone.next();

				if (medicine.getProductname().isEmpty() ||
						medicine.getGenericname().isEmpty()
						|| medicine.getCompanyname().isEmpty() ) 
				{
					mandatoryMissedList.add(medicine);
					medicineListListClone.remove();
				}
			}

			batchStore(medicineList);
		}
		catch (Exception e) {
			throw new FileUploadFailedException(ApplicationConstants.File_Upload_Failed);
		}
		return mandatoryMissedList;
	}

	private File convertMultiPartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public Boolean batchStore(List<Medicine> medicinelist) {
		 medicineRepository.saveAll(medicinelist);
		 return true;
	}

	public List<Medicine> getmedicinelist() {
		return medicineRepository.findAll();
	}
}