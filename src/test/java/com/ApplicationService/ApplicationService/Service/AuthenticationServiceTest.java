package com.ApplicationService.ApplicationService.Service;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collection;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.applicationservice.json.AuthenticationRequest;
import com.applicationservice.repository.IMedicineRepository;
import com.applicationservice.service.AuthenticationService;
import com.applicationservice.service.JwtRequestFilterService;
import com.applicationservice.service.JwtTokenService;
import com.applicationservice.service.MyuserDetailsService;

import ch.qos.logback.core.status.Status;


public class AuthenticationServiceTest {
	private UserDetails userDetails;
	
	private AuthenticationRequest authenticationRequest;
	
	@Spy
	private MyuserDetailsService userDetailsService;
	
	@Spy
	private Authentication authentication;
	
	
	@Before
	public void setup() {
		authenticationRequest=new AuthenticationRequest();
		authenticationRequest.setUid("sajibcse42");
		authenticationRequest.setPassword("1234");
		
		userDetails=new UserDetails() {
			
			@Override
			public boolean isEnabled() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean isCredentialsNonExpired() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean isAccountNonLocked() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean isAccountNonExpired() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public String getUsername() {
				// TODO Auto-generated method stub
				return "sajibcse42";
			}
			
			@Override
			public String getPassword() {
				// TODO Auto-generated method stub
				return "1234";
			}
			
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				// TODO Auto-generated method stub
				return null;
			}
		};
			
		
		userDetailsService = Mockito.spy(new MyuserDetailsService());
		
	} 
	
	@Test
	public void testAuthenticationcheck(){
				
		MyuserDetailsService userDetailsService = Mockito.mock(MyuserDetailsService.class);
		Mockito.when(userDetailsService.loadUserByUsername(authenticationRequest.getUid())).thenReturn(userDetails);
		
				
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUid());
		Assert.assertEquals(userDetails.getUsername(),"sajibcse42" );

	}

}
