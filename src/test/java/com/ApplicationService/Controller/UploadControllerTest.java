package com.ApplicationService.Controller;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.applicationservice.entity.Medicine;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UploadControllerTest {


	@Autowired 
	private TestRestTemplate restTemplate;
	private HttpHeaders headers;
	
	private List<Medicine> medicines;
	private Medicine medicine_one;
	private Medicine medicine_two;

	private static final String END_POINT_Upload = "/upload";
	private static final String END_POINT_GetAll_Medecines = "/AllMedicineData";

	@Before
	public void setUp() {
		medicine_one = new Medicine();
		medicine_one.setId(1L);
		medicine_one.setCompanyname("The ACME Laboratories Ltd.");
		medicine_one.setGenericname("Thiamine Hydrochloride");
		medicine_one.setProductname("A B1 100 mg Tablet");
		
		medicine_two = new Medicine();
		medicine_two.setId(1L);
		medicine_two.setCompanyname("The ACME Laboratories Ltd.");
		medicine_two.setGenericname("Thiamine Hydrochloride");
		medicine_two.setProductname("A B1 100 mg Tablet");
		
		medicines=Arrays.asList(medicine_one,medicine_two);	
		headers = null;
	}
	
	@Test
	public void testUploadController() throws Exception {
		String message = "All Data Uploaded Successfully";
		
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(END_POINT_Upload, 
				HttpMethod.POST, new HttpEntity<String>(null, headers), String.class);

		assertEquals(message, "All Data Uploaded Successfully");
	}
	
	@Test
	public void testGetMedicines() throws Exception {
		
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(END_POINT_GetAll_Medecines, 
				HttpMethod.GET, new HttpEntity<List<Medicine>>(medicines, headers), String.class);

		Assert.assertEquals(medicines.size(), 2);
	}
}
