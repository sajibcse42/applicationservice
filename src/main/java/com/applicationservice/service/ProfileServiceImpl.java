package com.applicationservice.service;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.applicationservice.entity.User;
import com.applicationservice.json.ProfileRequest;
import com.applicationservice.json.ProfileResponse;
import com.applicationservice.repository.IUserRepository;

@Service
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private IUserRepository iUserRepository;
	@Autowired
	private PasswordEncoder passEncoder;

	@Override
	public ProfileResponse addProfile(ProfileRequest request) {
		User user = new User();
		user.setEmail(request.getEmail());
		user.setPhoneNumber(request.getPhoneNumber());
		user.setPassword(passEncoder.encode(request.getPassword()));
		user.setType(request.getType());
		user.setUid(request.getUid());
		user.setCreatedAt(Instant.now());
		user.setUpdatedAt(Instant.now());
		user = iUserRepository.save(user);
		
		ProfileResponse response = new ProfileResponse();
		response.setId(user.getId());
		return response;
		
	}

	@Override
	public ProfileResponse updateProfile(ProfileRequest request) {
		// TODO Auto-generated method stub
		User user = new User();
		user=iUserRepository.findByemail(request.getEmail());
		user.setEmail(request.getEmail());
		user.setPhoneNumber(request.getPhoneNumber());
		user.setPassword(passEncoder.encode(request.getPassword()));
		user.setType(request.getType());
		user.setUid(request.getUid());
		user.setCreatedAt(Instant.now());
		user.setUpdatedAt(Instant.now());
		user = iUserRepository.save(user);
		ProfileResponse response = new ProfileResponse();
		response.setId(user.getId());
		return response;
	}

	@Override
	public ProfileRequest getProfile(Long profileId) {
		// TODO Auto-generated method stub
		User user = iUserRepository.findById(profileId).get();
		ProfileRequest profileRequest=new ProfileRequest();
		profileRequest.setEmail(user.getEmail());
		profileRequest.setPhoneNumber(user.getPhoneNumber());
		return profileRequest;
	}

	@Override
	public void deleteProfile(Long profileId) {
		// TODO Auto-generated method stub
		iUserRepository.deleteById(profileId);
				
	}

}