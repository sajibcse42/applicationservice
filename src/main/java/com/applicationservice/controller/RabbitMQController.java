package com.applicationservice.controller;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.applicationservice.json.Employee;
import com.applicationservice.json.ProfileRequest;
import com.applicationservice.service.ProfileService;
import com.applicationservice.service.RabbitMQSender;


@RestController
@RequestMapping(value = "/RabbitMQ/")
public class RabbitMQController {

	@Autowired
	RabbitMQSender rabbitMQSender;
	@Autowired
	ProfileService profileService;
	
	@GetMapping(value = "/producer")
	public String producer(@RequestParam("empName") String empName,@RequestParam("empId") String empId) 
	{
		Employee emp=new Employee();
		emp.setEmpId(empId);
		emp.setEmpName(empName);
		rabbitMQSender.send(emp);
		return "Message sent to the RabbitMQ  Successfully";
	}
	
	@RabbitListener(queues = "${RabbitMq.queue}")
	public Boolean listener(Employee employee) {
		//test purpose data saving..
		ProfileRequest model=new ProfileRequest();
		model.setUid(employee.getEmpId());
		model.setEmail("test@ymail.com");
		model.setPassword("1234");
		model.setPhoneNumber("01675128246");
		model.setType("user");
		
		profileService.addProfile(model);
		System.out.println("Recieved and data saved  From RabbitMQ: " + employee);
		return true;
	}
	
}
