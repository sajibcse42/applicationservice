package com.applicationservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.applicationservice.entity.Medicine;
import com.applicationservice.entity.User;


@Repository
public interface IMedicineRepository extends JpaRepository<Medicine, Long>{
	List<Medicine> findAll();
	
}

