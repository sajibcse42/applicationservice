package com.applicationservice.service.exception;

public class FileUploadFailedException extends RuntimeException {

    private static final long serialVersionUID = 6825537414445573470L;

    public FileUploadFailedException(String message) {
        super(message);
    }
}