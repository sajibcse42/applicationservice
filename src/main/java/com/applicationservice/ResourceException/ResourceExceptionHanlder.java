package com.applicationservice.ResourceException;

import java.time.Instant;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.applicationservice.entity.ErrorResponse;
import com.applicationservice.service.exception.AppAuthException;
import com.applicationservice.service.exception.FileUploadFailedException;
import com.applicationservice.service.exception.RabbitMQSerderException;

@ControllerAdvice
public class ResourceExceptionHanlder {
	@ExceptionHandler(FileUploadFailedException.class)
	@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
	public ErrorResponse FileUploadFailed(FileUploadFailedException e, HttpServletRequest r) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTitle(e.getMessage());
		errorResponse.setTimestamp(Instant.now().toEpochMilli());
		return errorResponse;
    }
	@ExceptionHandler(RabbitMQSerderException.class)
	@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
	public ErrorResponse RabbitMqSender(RabbitMQSerderException e, HttpServletRequest r) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTitle(e.getMessage());
		errorResponse.setTimestamp(Instant.now().toEpochMilli());
		return errorResponse;
    }
	
	@ExceptionHandler(AppAuthException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ErrorResponse AuthenticationException(AppAuthException e, HttpServletRequest r) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTitle(e.getMessage());
		errorResponse.setTimestamp(Instant.now().toEpochMilli());
		return errorResponse;
    }
	
}
