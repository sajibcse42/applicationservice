package com.ApplicationService.Controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.applicationservice.json.AuthenticationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthtenticationControllerTest {
	
	@Autowired 
	private TestRestTemplate restTemplate;
	private static final String END_POINT_Login= "/Login";
	private HttpHeaders headers;
	
	
	@Before
	public void setUp() {
		
		headers = null;
	}

	@Test
	public void testLogin() throws Exception {
		AuthenticationRequest login=new AuthenticationRequest();
		login.setUid("sajibcse42");
		login.setPassword("1234");
		
		
		assertEquals(login, true);
	}
}
