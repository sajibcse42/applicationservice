package com.applicationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.applicationservice.json.AuthenticationRequest;
import com.applicationservice.json.AuthenticationResponse;
import com.applicationservice.service.AuthenticationService;



@RestController
public class AuthenticationController {
	@Autowired
	private AuthenticationService AuthenticationServiceImpl;
		
	@RequestMapping(value = "/Login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

		String jwt=AuthenticationServiceImpl.Authenticationcheck(authenticationRequest);

		return ResponseEntity.ok(new AuthenticationResponse(jwt));
		
	}
}