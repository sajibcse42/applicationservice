package com.ApplicationService.ApplicationService.Service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.Spy;

import com.applicationservice.entity.Medicine;
import com.applicationservice.repository.IMedicineRepository;
import com.applicationservice.service.UploadService;

public class UploadServiceTest {
	private List<Medicine> medicines;
	private Medicine medicine_one;
	private Medicine medicine_two;
	
	@Spy
	private UploadService uploadService;
	
	@Before
	public void setup() {
		medicine_one = new Medicine();
		medicine_one.setId(1L);
		medicine_one.setCompanyname("The ACME Laboratories Ltd.");
		medicine_one.setGenericname("Thiamine Hydrochloride");
		medicine_one.setProductname("A B1 100 mg Tablet");
		
		medicine_two = new Medicine();
		medicine_two.setId(1L);
		medicine_two.setCompanyname("The ACME Laboratories Ltd.");
		medicine_two.setGenericname("Thiamine Hydrochloride");
		medicine_two.setProductname("A B1 100 mg Tablet");
		
		medicines=Arrays.asList(medicine_one,medicine_two);		
		
		uploadService = Mockito.spy(new UploadService());
	} 
	
	@Test
	public void testGetAllMedicines() {
		IMedicineRepository medicineRepository = Mockito.mock(IMedicineRepository.class);
		Mockito.when(medicineRepository.saveAll(medicines)).thenReturn(medicines);
		
		uploadService.medicineRepository = medicineRepository;
		List<Medicine> medicinelist = uploadService.getmedicinelist();
		
		Assert.assertEquals(medicines.size(), 2);
		
	}
	
	@Test
	public void testSaveAllMedicines() {
		IMedicineRepository medicineRepository = Mockito.mock(IMedicineRepository.class);
		Mockito.when(medicineRepository.saveAll(medicines)).thenReturn(medicines);
		
		uploadService.medicineRepository = medicineRepository;
		Boolean ok = uploadService.batchStore(medicines);
		
		Assert.assertEquals(ok, true);
		
	}
}
