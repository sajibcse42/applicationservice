package com.applicationservice.config;

public class ApplicationConstants {
	 public static final String File_Upload_Failed = "Something went wrong,file upload failed";
	 public static final String Sending_RabbitMQ_Failed = "Sending to RabbitMQ Failed";
	 public static final String Receiving_RabbitMQ_Failed = "Receiving from RabbitMQ Failed";
	 public static final String Authentication_failed = "Your username and password is not correct";
	 public static final String Authentication_Username = "Your username is not correct";
	 public static final String Authentication_Password = "Your Password  is not correct";
	 public static final String Authentication_Token = "Your token  is Expired";
}
