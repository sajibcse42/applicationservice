package com.ApplicationService.ApplicationService.Service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.applicationservice.json.Employee;
import com.applicationservice.service.RabbitMQSender;

public class RabbitMQService {
	private Employee emplyee;
	@Spy
	private RabbitMQSender rabbitMQSenderService;
	@Value("${RabbitMq.exchange}")
	private String exchange;
	
	@Value("${RabbitMq.routingkey}")
	private String routingkey;	
	@Before
	public void setup() {
		emplyee = new Employee();
		emplyee.setEmpId("Test001");
		emplyee.setEmpName("SajibAgent47");
				
		rabbitMQSenderService = Mockito.spy(new RabbitMQSender());		
		
	} 
	
	@Test
	public void testRabbitMqSender() {
		AmqpTemplate amqpTemplate = Mockito.mock(AmqpTemplate.class);
		amqpTemplate.convertAndSend(exchange, routingkey, emplyee);
		
		rabbitMQSenderService.amqpTemplate = amqpTemplate;
		
		Boolean ok = rabbitMQSenderService.send(emplyee);
		
		Assert.assertEquals(ok,true);
		
	}
	@Test
	public void testRabbitMqListener() {
		AmqpTemplate amqpTemplate = Mockito.mock(AmqpTemplate.class);
		amqpTemplate.convertAndSend(exchange, routingkey, emplyee);
		
		rabbitMQSenderService.amqpTemplate = amqpTemplate;
		
		Boolean ok = rabbitMQSenderService.send(emplyee);
		
		Assert.assertEquals(ok,true);
		
	}
}
