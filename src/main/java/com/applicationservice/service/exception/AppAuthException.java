package com.applicationservice.service.exception;

public class AppAuthException extends RuntimeException {

    private static final long serialVersionUID = 6825537414445573470L;

    public AppAuthException(String message) {
        super(message);
    }
}