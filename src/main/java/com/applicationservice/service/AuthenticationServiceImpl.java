package com.applicationservice.service;

import javax.security.sasl.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.applicationservice.config.ApplicationConstants;
import com.applicationservice.json.AuthenticationRequest;



@Service
public class AuthenticationServiceImpl implements AuthenticationService{
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private MyuserDetailsService userDetailsService;
	
	@Autowired 
	private JwtTokenService jwtTokenService;
	
	@Override
	public String Authenticationcheck(AuthenticationRequest authenticationRequest) {
		
		
			Authentication isvaild=authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authenticationRequest.getUid(), authenticationRequest.getPassword()));
		
			
			final UserDetails userDetails = userDetailsService
					.loadUserByUsername(authenticationRequest.getUid());

			final String jwt = jwtTokenService.generateToken(userDetails);
			return jwt;
			
		}
	}
		

