package com.ApplicationService.Controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.applicationservice.json.Employee;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RabbitMqControllerTest {
	@Autowired 
	private TestRestTemplate restTemplate;
	private HttpHeaders headers;
	private static final String END_POINT_RabbitMq_Sender = "/RabbitMQ/producer";
	
	@Before
	public void setUp() {
		headers = null;
	}
	
	@Test
	public void testRabbitMqProducer() throws Exception {
		Employee emp=new Employee();
		emp.setEmpId("test_01");
		emp.setEmpName("test");
		String message = "Message sent to the RabbitMQ  Successfully";
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(END_POINT_RabbitMq_Sender, 
				HttpMethod.GET, new HttpEntity<Employee>(emp, headers), String.class);

		assertEquals(message, "Message sent to the RabbitMQ  Successfully");
	}
	
	
	
	
}
