package com.applicationservice.service;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.applicationservice.config.ApplicationConstants;
import com.applicationservice.entity.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component

public class JwtTokenService {
	 private String SECRET_KEY = "secret";
	 
	 @Autowired
	    private UserDetailsService userDetailsService;

	 
	 public User parseToken(String token) {
	        try {
	            Claims body = Jwts.parser()
	                    .setSigningKey(SECRET_KEY)
	                    .parseClaimsJws(token)
	                    .getBody();

	            User u = new User();
	            u.setUid((String) body.get("uid"));
	            u.setPassword((String) body.get("password"));
	            //u.setRole((String) body.get("role"));

	            return u;

	        } catch (Exception e) {
	            return null;
	        }
	    }

	    public String extractUsername(String token) {
	        return extractClaim(token, Claims::getSubject);
	    }

	    public Date extractExpiration(String token) {
	        return extractClaim(token, Claims::getExpiration);
	    }

	    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
	        final Claims claims = extractAllClaims(token);
	        return claimsResolver.apply(claims);
	    }
	    private Claims extractAllClaims(String token) {
	        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
	    }

	    private Boolean isTokenExpired(String token) {
	        return extractExpiration(token).before(new Date());
	    }

	    public String generateToken(UserDetails userDetails) {
	        Map<String, Object> claims = new HashMap<>();
	        return createToken(claims, userDetails.getUsername());
	    }

	    private String createToken(Map<String, Object> claims, String subject) {

	        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
	                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
	                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
	    }

	   
	    
	    public String resolveToken(HttpServletRequest req) {
	        String bearerToken = req.getHeader("Authorization");
	        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
	            return bearerToken.substring(7, bearerToken.length());
	        }
	        return null;
	    }
	    
	    public boolean validateToken(String token) throws AuthenticationException {
	        try {
	            Jws<Claims> claims = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);

	            if (claims.getBody().getExpiration().before(new Date())) {
	                return false;
	            }

	            return true;
	        } catch (JwtException | IllegalArgumentException e)
	        {
	        	 //return false;
	            throw new AuthenticationException(ApplicationConstants.Authentication_Token);
	          
	        }
	    }
	    
	    public UsernamePasswordAuthenticationToken getAuthentication(String token) {
	        UserDetails userDetails = this.userDetailsService.loadUserByUsername(getUsername(token));
	        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	    }
	    
	    public String getUsername(String token) {
	        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().getSubject();
	    }
}
