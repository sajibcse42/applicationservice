package com.ApplicationService.Controller;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.applicationservice.entity.Medicine;
import com.applicationservice.repository.IMedicineRepository;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloWorldControllerTest {
	
	@Autowired 
	private TestRestTemplate restTemplate;
	private static final String END_POINT_Hello= "/Hello";
	private HttpHeaders headers;
	
	@Before
	public void setUp() {
		
		headers = null;
	}
	
	@Test
	public void testHelloWorld() throws Exception {
		String message = "Hello World";
		
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(END_POINT_Hello, 
				HttpMethod.GET, new HttpEntity<String>(null, headers), String.class);

		assertEquals(message, "Hello World");
	}
}
