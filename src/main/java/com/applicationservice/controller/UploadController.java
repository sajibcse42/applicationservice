package com.applicationservice.controller;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.applicationservice.entity.Medicine;
import com.applicationservice.service.UploadService;

@RestController
public class UploadController {
	@Autowired
	private UploadService uploadService;

	@Autowired
	UploadController(UploadService uploadService) {
		this.uploadService = uploadService;
	}

	@PostMapping(value = "/upload")
	public String uploadFile(@RequestPart(value = "file") MultipartFile file) throws IOException {
		 uploadService.uploadFile(file);
		 return "All Data Uploaded Successfully";
	}
	
	@RequestMapping(value = "/AllMedicineData", method = RequestMethod.GET)
	public List<Medicine> getMedicines(){
		return uploadService.getmedicinelist();
	}
}
